#include<iostream>
#include"CACode.h"

using namespace std;

int main()
{
	int G1x[10] = { 0 }, G2x[10] = { 0 };//10级G1发生器寄存器，10级G2发生器寄存器
	int prnCA[32] = { 0 };//32颗GPS卫星的C/A码某个码片值
	char prnCAstr[32][10240] = {'\0'};//32颗GPS卫星的C/A码码片值
	char fn[] = "PRNCA.txt";//将32颗GPS卫星的C/A码码片值写入文件的文件名
	FILE *fp = NULL;//文件指针

	if (fopen_s(&fp, fn, "w"))//打开文件
	{
		printf("Fail to open the file!\n");
		return -1;
	}

	initX(G1x, G2x);//初始化

	for (int i = 0; i < 2046; i++)
	{
		PRNCAGenerate(G1x[9], G2x, prnCA);//C/A码发生器
		G1Generate(G1x);//G1发生器
		G2Generate(G2x);//G2发生器


		for (int j = 0; j < 32; j++)
		{
			prnCAstr[j][i] = prnCA[j];//将C/A码码片值存储
		}
	}

	//写入文件
	for (int i = 0; i < 32; i++)
	{
		for (int j = 0; j < 2046; j++)
		{
			fprintf(fp,"%d", prnCAstr[i][j]);
		}
		fprintf(fp,"\n");
	}

	fclose(fp);
	return 0;
}