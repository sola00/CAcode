#include "CACode.h"


/*
* 介绍	G1发生器和G2发生器初始化：将G1x和G2x全部置为1
* 输入	G1x[10]		G1发生器的10级寄存器
* 输入	G2x[10]		G2发生器的10级寄存器
* 输出	G1x[10]		初始化后的G1发生器的10级寄存器
* 输出	G2x[10]		初始化后的G2发生器的10级寄存器
* 返回	void
*/
void initX(int G1x[10], int G2x[10])
{
	for (int i = 0; i < 10; i++)
	{
		G1x[i] = G2x[i] = 1;
	}
}

/*
* 介绍	10级G1发生器
* 输入	x[10]	10级寄存器
* 输出	x[10]	一次移位后的10级寄存器
* 返回	void
*/
void G1Generate(int x[10])
{
	int temp= x[2] ^ x[9];
	for (int i = 9; i > 0; i--)
	{
		x[i] = x[i - 1];
	}
	x[0] = temp;
}

/*
* 介绍	10级G2发生器
* 输入	x[10]	10级寄存器
* 输出	x[10]	一次移位后的10级寄存器
* 返回	void
*/
void G2Generate(int x[10])
{
	int temp = x[1] ^ x[2] ^ x[5] ^ x[7] ^ x[8] ^ x[9];
	for (int i = 9; i > 0; i--)
	{
		x[i] = x[i - 1];
	}
	x[0] = temp;
}

/*
* 介绍	C/A码发生器
* 输入	G1			G1发生器的第10级寄存器的值G1x[9]
* 输入	G2x[10]		G2发生器的10级寄存器
* 输出	prnCA[32]	由G1发生器和G2发生器运算得到GPS 32颗卫星的C/A码码片值
* 返回	void
*/
void PRNCAGenerate(const int G1, const int G2x[10], int prnCA[32])
{
	prnCA[0] = G1 ^ (G2x[1] ^ G2x[5]);
	prnCA[1] = G1 ^ (G2x[2] ^ G2x[6]);
	prnCA[2] = G1 ^ (G2x[3] ^ G2x[7]);
	prnCA[3] = G1 ^ (G2x[4] ^ G2x[8]);
	prnCA[4] = G1 ^ (G2x[0] ^ G2x[8]);
	prnCA[5] = G1 ^ (G2x[1] ^ G2x[9]);
	prnCA[6] = G1 ^ (G2x[0] ^ G2x[7]);
	prnCA[7] = G1 ^ (G2x[1] ^ G2x[8]);
	prnCA[8] = G1 ^ (G2x[2] ^ G2x[9]);
	prnCA[9] = G1 ^ (G2x[1] ^ G2x[2]);
	prnCA[10] = G1 ^ (G2x[2] ^ G2x[3]);
	prnCA[11] = G1 ^ (G2x[4] ^ G2x[5]);
	prnCA[12] = G1 ^ (G2x[5] ^ G2x[6]);
	prnCA[13] = G1 ^ (G2x[6] ^ G2x[7]);
	prnCA[14] = G1 ^ (G2x[7] ^ G2x[8]);
	prnCA[15] = G1 ^ (G2x[8] ^ G2x[9]);
	prnCA[16] = G1 ^ (G2x[0] ^ G2x[3]);
	prnCA[17] = G1 ^ (G2x[1] ^ G2x[4]);
	prnCA[18] = G1 ^ (G2x[2] ^ G2x[5]);
	prnCA[19] = G1 ^ (G2x[3] ^ G2x[6]);
	prnCA[20] = G1 ^ (G2x[4] ^ G2x[7]);
	prnCA[21] = G1 ^ (G2x[5] ^ G2x[8]);
	prnCA[22] = G1 ^ (G2x[0] ^ G2x[2]);
	prnCA[23] = G1 ^ (G2x[3] ^ G2x[5]);
	prnCA[24] = G1 ^ (G2x[4] ^ G2x[6]);
	prnCA[25] = G1 ^ (G2x[5] ^ G2x[7]);
	prnCA[26] = G1 ^ (G2x[6] ^ G2x[8]);
	prnCA[27] = G1 ^ (G2x[7] ^ G2x[9]);
	prnCA[28] = G1 ^ (G2x[0] ^ G2x[5]);
	prnCA[29] = G1 ^ (G2x[1] ^ G2x[6]);
	prnCA[30] = G1 ^ (G2x[2] ^ G2x[7]);
	prnCA[31] = G1 ^ (G2x[3] ^ G2x[8]);
}