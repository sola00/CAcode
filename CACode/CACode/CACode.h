#ifndef CACODE_H_
#define CACODE_H_

/*
* 介绍	G1发生器和G2发生器初始化：将G1x和G2x全部置为1
* 输入	G1x[10]		G1发生器的10级寄存器
* 输入	G2x[10]		G2发生器的10级寄存器
* 输出	G1x[10]		初始化后的G1发生器的10级寄存器
* 输出	G2x[10]		初始化后的G2发生器的10级寄存器
* 返回	void
*/
void initX(int G1x[10], int G2x[10]);

/*
* 介绍	10级G1发生器
* 输入	x[10]	10级寄存器
* 输出	x[10]	一次移位后的10级寄存器
* 返回	void
*/
void G1Generate(int x[10]);

/*
* 介绍	10级G2发生器
* 输入	x[10]	10级寄存器
* 输出	x[10]	一次移位后的10级寄存器
* 返回	void
*/
void G2Generate(int x[10]);

/*
* 介绍	C/A码发生器
* 输入	G1			G1发生器的第10级寄存器的值G1x[9]
* 输入	G2x[10]		G2发生器的10级寄存器
* 输出	prnCA[32]	由G1发生器和G2发生器运算得到GPS 32颗卫星的C/A码码片值
* 返回	void
*/
void PRNCAGenerate(const int G1, const int G2x[10], int prnCA[32]);


#endif // !CACODE_H_

